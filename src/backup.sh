#! /bin/bash -f

# must be root to ensure no access issues
if [[ `whoami` != root ]]; then
	echo "Please run this script using root."
	exit
fi
this=${PWD}

echo "Starting Backup."
backup_dir="/__DROPBOX_BACKUP/"
date=`date +%F`
mkdir $backup_dir$date
chmod 755 $date

echo "Backing Up..."
rsync -raz /home/andrew/Dropbox/ "${backup_dir}${date}/"

echo "Saving Progress..."
# change directories for relative path
cd $backup_dir
tar -zcf "${date}.tar.gz" $date
chmod 755 $date
# change directories back
cd $this

echo "Cleaning up..."
rm -R $backup_dir$date


echo "Done."
